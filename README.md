# ValkyrieJB

A semi-untethered jailbreak for iOS 13.0-13.7

## Exploits used

FTSB, an exploit for iOS 13.0-13.7, developed by [someone that you used to know.](someone) <br>
Compatible with Apple A9-A13 processors.

## Licensing

Valkyrie is hereby licensed under the BSD3-Clause license, viewable [here](https://gitlab.com/ted_kaczynski/valkyrie/-/blob/main/LICENSE)

## Credits

[Mineek](https://github.com/mineek/) for a lot of help.<br>
[Odyssey Team](https://github.com/Odyssey-Team/) (more namely the [Odyssey](https://github.com/Odyssey-Team/Odyssey/) for being the bulk of this thing.) for licensing Odyssey under BSD-3, which allows redistribution, etc. A lot of Odyssey is used in Valkyrie, and this would not be possible without it.<br>
<br>
Whenever I find out who developed the FTSB exploit, I'll put them here.

