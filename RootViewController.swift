import UIKit

class RootViewController: UIViewController {

    var count: Int = 0
    let labelCount = UILabel()
    let alert = UIAlertController(title: "Notice", message: "This version currently does not jailbreak your device. \r\n Please wait for an update which has this functionality. \r\n - United States Department of Defense", preferredStyle: .alert)

    override func viewDidLoad() {
        super.viewDidLoad()

        let button = UIButton(type: .system)
        button.setTitle("Start", for: .normal)
        button.addTarget(self, action: #selector(counter), for: .touchUpInside)
        
        let label = UILabel()
        label.text = "Welcome to ValkyrieJB. \r\n Developed by the United States Department of Defense."
        label.textAlignment = .center
        
    //    labelCount.text = "\(count)"
    //    labelCount.textAlignment = .center
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok.", comment: "Default action"), style: .default, handler: resetCounter))

        button.translatesAutoresizingMaskIntoConstraints = false
        label.translatesAutoresizingMaskIntoConstraints = false
        labelCount.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(button)
        view.addSubview(label)
        view.addSubview(labelCount)
        
        NSLayoutConstraint.activate([
            button.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            button.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            
            label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            label.bottomAnchor.constraint(equalTo: button.topAnchor, constant: -16),
            
            labelCount.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            labelCount.bottomAnchor.constraint(equalTo: label.topAnchor, constant: -16),
        ])
    }
    
    @objc func counter() {
        count = count + 1
//        labelCount.text = "\(count)"
        if count == 1 {
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    @objc func resetCounter(action: UIAlertAction) {
        count = 0
//        labelCount.text = "\(count)"
    }
}
